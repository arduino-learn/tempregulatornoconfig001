void onNumberLeft(int a, int b, int c, int d, int e, int f, int g){
  if (a == 1){
    digitalWrite(3, HIGH);
  } else {
    digitalWrite(3, LOW);
  }
  if (b == 1){
    digitalWrite(4, HIGH);
  } else {
    digitalWrite(4, LOW);
  }
  if (c == 1){
    digitalWrite(10, HIGH);
  } else {
    digitalWrite(10, LOW);
  }
  if (d == 1){
    digitalWrite(9, HIGH);
  } else {
    digitalWrite(9, LOW);
  }
  if (e == 1){
    digitalWrite(8, HIGH);
  } else {
    digitalWrite(8, LOW);
  }
  if (f == 1){
    digitalWrite(1, HIGH);
  } else {
    digitalWrite(1, LOW);
  }
  if (g == 1){
    digitalWrite(0, HIGH);
  } else {
    digitalWrite(0, LOW);
  }
}
void onNumberRight(int a, int b, int c, int d, int e, int f, int g){
  if (a == 1){
    digitalWrite(6, HIGH);
  } else {
    digitalWrite(6, LOW);
  }
  if (b == 1){
    digitalWrite(7, HIGH);
  } else {
    digitalWrite(7, LOW);
  }
  if (c == 1){
    digitalWrite(13, HIGH);
  } else {
    digitalWrite(13, LOW);
  }
  if (d == 1){
    digitalWrite(12, HIGH);
  } else {
    digitalWrite(12, LOW);
  }
  if (e == 1){
    digitalWrite(11, HIGH);
  } else {
    digitalWrite(11, LOW);
  }
  if (f == 1){
    digitalWrite(5, HIGH);
  } else {
    digitalWrite(5, LOW);
  }
  if (g == 1){
    digitalWrite(4, HIGH);
  } else {
    digitalWrite(4, LOW);
  }
}
int* toLR(int in){
  if (in < 100){
    static int out[2];
    out[0] = in / 10;
    out[1] = in - (out[0] * 10);
    return out;
  }
}
void showNumberL(int o){                // A, B, C, D, E, F, G
  if (o==0){
    onNumberLeft(1, 1, 1, 1, 1, 1, 0);
  }
  if (o==1){
    onNumberLeft(0, 1, 1, 0, 0, 0, 0);
  }
  if (o==2){
    onNumberLeft(1, 1, 0, 1, 1, 0, 1);
  }
  if (o==3){
    onNumberLeft(1, 1, 1, 1, 0, 0, 1);
  }
  if (o==4){
    onNumberLeft(0, 1, 1, 0, 0, 1, 1);
  }
  if (o==5){
    onNumberLeft(1, 0, 1, 1, 0, 1, 1);
  }
  if (o==6){
    onNumberLeft(1, 0, 1, 1, 1, 1, 1);
  }
  if (o==7){
    onNumberLeft(1, 1, 1, 0, 0, 0, 0);
  }
  if (o==8){
    onNumberLeft(1, 1, 1, 1, 1, 1, 1);
  }
  if (o==9){
    onNumberLeft(1, 1, 1, 1, 0, 1, 1);
  }
}
void showNumberR(int n){
  if (n==0){
    onNumberRight(1, 1, 1, 1, 1, 1, 0);
  }
  if (n==1){
    onNumberRight(0, 1, 1, 0, 0, 0, 0);
  }
  if (n==2){
    onNumberRight(1, 1, 0, 1, 1, 0, 1);
  }
  if (n==3){
    onNumberRight(1, 1, 1, 1, 0, 0, 1);
  }
  if (n==4){
    onNumberRight(0, 1, 1, 0, 0, 1, 1);
  }
  if (n==5){
    onNumberRight(1, 0, 1, 1, 0, 1, 1);
  }
  if (n==6){
    onNumberRight(1, 0, 1, 1, 1, 1, 1);
  }
  if (n==7){
    onNumberRight(1, 1, 1, 0, 0, 0, 0);
  }
  if (n==8){
    onNumberRight(1, 1, 1, 1, 1, 1, 1);
  }
  if (n==9){
    onNumberRight(1, 1, 1, 1, 0, 1, 1);
  }
}
void displayNumber(int in){
  if (in < 100){
    showNumberL(in / 10);
    showNumberR(in - in / 10);
  }
}
void setup() {                
  // initialize the digital pin as an output.
  pinMode(PD0, OUTPUT);
  pinMode(PD1, OUTPUT);
  for (int i=3; i<14; i++){
    pinMode(i, OUTPUT);
  }
}

// the loop routine runs over and over again forever:
void loop() {
  //displayNumber(0);
  /*
  for (int i=0; i<10; i++){
    showNumberL(i);
    delay(250);
  }*/
  onNumberLeft(1, 1, 1, 1, 1, 1, 1);
}
